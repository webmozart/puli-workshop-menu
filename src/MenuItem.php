<?php

namespace Acme\Menu;

class MenuItem
{
    private $label;

    private $link;

    public function __construct($label, $link)
    {
        $this->label = $label;
        $this->link = $link;
    }

    public function getLabel()
    {
        return $this->label;
    }

    public function getLink()
    {
        return $this->link;
    }
}
