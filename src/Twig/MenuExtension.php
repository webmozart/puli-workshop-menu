<?php

namespace Acme\Menu\Twig;

use Acme\Menu\Menu;
use Twig_Extension;
use Twig_SimpleFunction;

class MenuExtension extends Twig_Extension
{
    private $menu;

    public function __construct(Menu $menu)
    {
        $this->menu = $menu;
    }

    public function getName()
    {
        return 'acme_menu';
    }

    public function getGlobals()
    {
        return array(
            'menu' => $this->menu,
        );
    }
}
