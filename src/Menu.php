<?php

namespace Acme\Menu;

class Menu
{
    private $items = array();

    public function addItem(MenuItem $item)
    {
        $this->items[] = $item;
    }

    public function getItems()
    {
        return $this->items;
    }
}
